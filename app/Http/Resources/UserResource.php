<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $product_level = $this->products_level;
        return [
            'id' => $this->id,
            'name' => $this->name,
            'mobile' => $this->mobile,
            'address' => $this->address,
            'status' => $this->status,
            'prepayment_amount' => $this->prepayment_amount,
            'prepayment_type' => $this->prepayment_type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'level' => calc_user_level($product_level),
            'products_level' => $product_level,
            'user_type' => 'user'
        ];
    }
}
