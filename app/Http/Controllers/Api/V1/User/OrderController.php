<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Api\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $query = auth('user')->user()->orders();

        $request->product_id ? $query->where('product_id', $request->product_id) : '';
        $request->type ? $query->where('type', $request->type) : '';
        $request->status ? $query->where('status', $request->status) : '';

        $query->with(['product' => function ($q) {
            $q->select('id', 'name');
        }]);

        $orders = $query->paginate($request->per_page ?? 50);
        return OrderResource::collection($orders);
    }

    public function show($order_id)
    {
        $order = auth('user')->user()->orders()
            ->with(['product' => function ($q) {
                $q->select('id', 'name');
            }])
            ->findOrFail($order_id);
        return new OrderResource($order);
    }

    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required|in:sell,buy',
            'amount' => 'required|numeric',
            'product_id' => 'required|exists:products,id',
        ]);
        try {
            $product = Product::query()->where('id', $request->product_id)->first();
            if ($request->amount > $product->limit) {
                $limit= $product->limit;
                $unit= $product->unit;
                return Response::error(" سقف سفارش {$limit} {$unit} است", 406);
            }
            auth('user')->user()->orders()->create([
                'type' => $request->type,
                'amount' => $request->amount,
                'product_id' => $request->product_id,
                'store_id' => $product->store_id,
                'price' => $product["{$request->type}_price"],
            ]);
            return Response::success();
        } catch (\Exception $exception) {
            return Response::error();
        }

    }
}
