<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Api\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Notifications\VerifySms;
use Illuminate\Support\Facades\App;
use Kavenegar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\ClientRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AuthController extends Controller
{

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'mobile' => 'numeric|unique:users,mobile'
        ]);
        try {
            User::query()->create([
                'name' => $request->name,
                'mobile' => $request->mobile
            ]);
            return Response::success();
        } catch (\Exception $exception) {
            return Response::error();
        }
    }


    public function login(Request $request)
    {
        $request->validate([
            'mobile' => 'required|numeric',
            'code' => 'nullable|numeric',
        ]);


//        $code= rand(10000,99999);
//        $result = Kavenegar::VerifyLookup($request->mobile, $code, 'verify');
//        dd($result);
        // This line removed
//        return $this->fake_login($request->mobile);

        try {
            if ($request->mobile && $request->code) {
                $user = User::query()
                    ->where('mobile', $request->mobile)
                    ->active()
                    ->firstOrFail();
                $code = $user->verify_code;

                if (!$code || $code != $request->code) {
                    return Response::error('کد تایید صحیح نمیباشد یا منقضی شده است', 401);
                }

                App::clearResolvedInstance(ClientRepository::class);
                app()->singleton(ClientRepository::class, function () {
                    return new ClientRepository(1, null); // You should give the client id in the first parameter
                });

                $token = $user->createToken('login-token')->accessToken;
                $user->deleteVerifyCode();
                return Response::success(data: [
                    'user' => new UserResource($user),
                    'token' => $token,
                    'token_type' => 'Bearer'
                ]);
            } else {
                $user = User::query()
                    ->where('mobile', $request->mobile)
                    ->active()
                    ->first();
                if (!$user) {
                    return Response::error('شماره مورد نظر وجود ندارد یا هنوز توسط ادمین تایید نشده است',404);
                }
                $code = $user->verify_code;
                if ($code) {
                    return Response::error('کد تایید یکبار برای شما ارسال شده است', 401);
                }
                $user->notify(new VerifySms());
                return Response::success(message: 'کد با موفقیت ارسال شد');
            }
        } catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    private function fake_login($mobile)
    {
        $user = User::query()
            ->where('mobile', $mobile)
            ->active()
            ->firstOrFail();
        $token = $user->createToken('login-token')->accessToken;
        return Response::success(data: [
            'user' => new UserResource($user),
            'token' => $token,
            'token_type' => 'Bearer'
        ]);
    }

    public function logout()
    {
        \auth('user')->user()->token()->revoke();
        return Response::success();
    }
}
