<?php

namespace App\Http\Controllers\Api\V1\Operator;

use App\Http\Controllers\Api\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $product = Product::query()->products_store()->get();
        return new ProductResource($product);
    }
}
