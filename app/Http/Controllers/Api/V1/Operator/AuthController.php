<?php

namespace App\Http\Controllers\Api\V1\Operator;

use App\Http\Controllers\Api\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\OperatorResource;
use App\Http\Resources\UserResource;
use App\Models\Operator;
use App\Models\User;
use App\Notifications\VerifySms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\ClientRepository;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'mobile' => 'required|numeric',
            'code' => 'nullable|numeric',
        ]);

        try {
            if ($request->mobile && $request->code) {
                $operator= Operator::query()
                    ->where('mobile',$request->mobile)
                    ->firstOrFail();

                $code = $operator->verify_code;

                if (!$code || $code != $request->code) {
                    return Response::error('کد تایید صحیح نمیباشد یا منقضی شده است', 401);
                }

                App::clearResolvedInstance(ClientRepository::class);
                app()->singleton(ClientRepository::class, function () {
                    return new ClientRepository(3, null); // You should give the client id in the first parameter
                });

                $token= $operator->createToken('login-token')->accessToken;
                $operator->deleteVerifyCode();
                return Response::success(data:[
                    'operator' => new OperatorResource($operator),
                    'token' => $token,
                    'token_type' => 'Bearer'
                ]);
            }else {
                $operator= Operator::query()->where('mobile',$request->mobile)->first();
                if (!$operator) {
                    return Response::error('شماره مورد نظر وجود ندارد',404);
                }
                $code = $operator->verify_code;
                if ($code) {
                    return Response::error('کد تایید یکبار برای شما ارسال شده است', 401);
                }
                $operator->notify(new VerifySms());
                return Response::success(message:'کد با موفقیت ارسال شد');
            }
        }catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    public function logout()
    {
        \auth('operator')->user()->token()->revoke();
        return Response::success();
    }
}
