<?php

namespace App\Http\Controllers\Api\V1\Operator;

use App\Http\Controllers\Api\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\SettingResource;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{

    /*
     * نمایش مقادیر تنظیمات عمومی
     */
    public function general()
    {
        $settings = Setting::query()->where('name', 'like', 'general_%')->get();
        return SettingResource::collection($settings);
    }

    /*
     * ذخیره و اپدیت تنظیمات عمومی
     */
    public function general_store(Request $request)
    {

        try {
            $items = [];
            foreach ($request->all() as $key => $value) {
                if (strstr($key, 'general_') === false)
                    continue;
                $value = is_array($value) ? json_encode($value) : $value;
                $items[] = [
                    'name' => $key,
                    'value' => $value
                ];
            }
            if (count($items) > 0)
                Setting::query()->upsert($items, ['name'], ['value']);
            return Response::success();
        } catch (\Exception $exception) {
            return Response::error();
        }
    }
}
