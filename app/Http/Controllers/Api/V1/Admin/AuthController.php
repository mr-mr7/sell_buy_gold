<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Api\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\AdminResource;
use App\Http\Resources\UserResource;
use App\Models\Admin;
use App\Models\User;
use App\Notifications\VerifySms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Laravel\Passport\ClientRepository;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'mobile' => 'required|numeric',
            'code' => 'nullable|numeric',
        ]);

        try {
            if ($request->mobile && $request->code) {
                /** @var Admin $admin */
                $admin= Admin::query()
                    ->where('mobile',$request->mobile)
                    ->firstOrFail();

                $code = $admin->verify_code;

                if (!$code || $code != $request->code) {
                    return Response::error('کد تایید صحیح نمیباشد یا منقضی شده است', 401);
                }

                App::clearResolvedInstance(ClientRepository::class);
                app()->singleton(ClientRepository::class, function () {
                    return new ClientRepository(2, null); // You should give the client id in the first parameter
                });

                $token= $admin->createToken('login-token')->accessToken;
                $admin->deleteVerifyCode();
                return Response::success(data:[
                    'admin' => new AdminResource($admin),
                    'token' => $token,
                    'token_type' => 'Bearer'
                ]);

            }else {
                $admin= Admin::query()->where('mobile',$request->mobile)->first();
                if (!$admin) {
                    return Response::error('شماره مورد نظر وجود ندارد',404);
                }
                $code = $admin->verify_code;
                if ($code) {
                    return Response::error('کد تایید یکبار برای شما ارسال شده است', 401);
                }
                $admin->notify(new VerifySms());
                return Response::success(message:'کد با موفقیت ارسال شد');
            }
        }catch (\Exception $exception) {
            return Response::error($exception->getMessage());
        }
    }

    private function fake_login($mobile) {
        $admin = Admin::query()
            ->where('mobile', $mobile)
            ->firstOrFail();
        $token = $admin->createToken('login-token')->accessToken;
        return Response::success(data: [
            'admin' => new AdminResource($admin),
            'token' => $token,
            'token_type' => 'Bearer'
        ]);
    }

    public function logout()
    {
        \auth('admin')->user()->token()->revoke();
        return Response::success();
    }
}
