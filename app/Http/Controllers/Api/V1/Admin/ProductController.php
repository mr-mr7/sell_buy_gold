<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Events\ProductChangePrice;
use App\Http\Controllers\Api\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $product = Product::query()->products_store()->get();
        return new ProductResource($product);
    }

    public function update(Request $request,$product_id)
    {
        $request->validate([
            'sell_price' => 'required|numeric|min:0',
            'buy_price' => 'required|numeric|min:0',
        ]);
        $product= Product::query()
            ->where('id', $product_id)
            ->products_store()
            ->first();
        $update = $product->update($request->only('sell_price', 'buy_price'));
        broadcast(new ProductChangePrice($product));
        return $update ? Response::success() : Response::error();
    }
}
