<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Api\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\SettingResource;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $settings= Setting::all();
        return SettingResource::collection($settings);
    }

    public function update(Request $request)
    {
        $request->validate([
            '*.name' => 'in:site_status,title'
        ]);
        try {
            foreach ($request->all() as $item) {
                // این قسمت در اینده وقتی تنظیمات زیاد تر شد میتونه خیلی بهینه تر بشه
                Setting::query()->where('name',$item['name'])
                    ->update(['value'=>$item['value']]);

            }
            return Response::success();
        }catch (\Exception $exception) {
            return Response::error();
        }
    }
}
