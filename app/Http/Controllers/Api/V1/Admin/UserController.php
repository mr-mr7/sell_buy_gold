<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Events\UserChangeStatus;
use App\Http\Controllers\Api\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserBotResource;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Telegram\Bot\Laravel\Facades\Telegram;

class UserController extends Controller
{
    // گرفتن لیست کاربران سایت
    public function index(Request $request)
    {
        $query = User::query();

        $request->name ? $query->where('name', 'like', "%{$request->name}%") : '';
        $request->mobile ? $query->where('mobile', 'like', "%{$request->mobile}%") : '';
        $request->address ? $query->where('address', 'like', "%{$request->address}%") : '';
        $request->status ? $query->where('status', $request->status) : '';
        $request->created_at ? $query->where('created_at', $request->created_at) : '';

        $users_bot = $query->paginate($request->per_page ?? 50);
        return UserResource::collection($users_bot);
    }

    // نمایش اطلاعات یک کاربر ربات
    public function show(User $user)
    {
        return new UserResource($user);
    }

    // تایید یا رد کردن کاربران ربات برای دسترسی به ربات
    public function confirm_reject(Request $request, User $user)
    {
        $request->validate([
            'status' => 'required|in:-1,1',
        ]);
        try {
            $user->status = $request->status;
            $user->save();
            UserChangeStatus::dispatch($user);
            return Response::success();
        } catch (\Exception $exception) {
            return Response::error();
        }
    }

    // ویرایش اطلاعات کاربر
    public function update(Request $request, User $user)
    {
        $request->validate([
            'mobile' => 'unique:users,mobile,'.$user->id
        ]);
        $user->update($request->only(['mobile','name','address','prepayment_amount','prepayment_type']));
        return Response::success();
    }
}
