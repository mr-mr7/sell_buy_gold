<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Api\Response;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LevelController extends Controller
{
    public function index()
    {
        $start_at = \request()->start_at ? \request()->start_at : Carbon::now()->subMonth();
        $end_at = \request()->end_at ? \request()->end_at : Carbon::now();
        $order = Order::query()
            ->withoutGlobalScopes()
            ->whereBetween('orders.created_at', [$start_at, $end_at])
            ->groupBy('type', 'product_id')
            ->join('products', 'products.id', 'orders.product_id')
            ->select([
                'products.id as product_id',
                'products.name as product_name',
                'products.unit as product_unit',
                'type',
                DB::raw('count(*) as count'),
                DB::raw('sum(' . DB::raw('amount * ' . DB::raw('IF(orders.type="buy", 1, -1)')) . ') as total_amount'),
                DB::raw('sum(' . DB::raw('amount * price * ' . DB::raw('IF(orders.type="buy", 1, -1)')) . ') as total_price')
            ])->get();
        return Response::success($order);
    }
}
