<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Events\OrderChangeStatus;
use App\Http\Controllers\Api\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Morilog\Jalali\Jalalian;
use Telegram\Bot\Laravel\Facades\Telegram;

class OrderController extends Controller
{
    // نمایش سفارشات کاربران فروشگاه
    public function index(Request $request)
    {
        $query = Order::query()->admin_order();

        $request->name ? $query->whereHas('user', function ($q) use ($request) {
            $q->where('name', 'like', "%{$request->name}%");
        }) : '';
        $request->mobile ? $query->whereHas('user', function ($q) use ($request) {
            $q->where('mobile', 'like', "%{$request->mobile}%");
        }) : '';
        $request->product_id ? $query->where('product_id', $request->product_id) : '';
        $request->type ? $query->where('type', $request->type) : '';
        $request->status ? $query->where('status', $request->status) : '';

        $query->with(['user' => function ($q) {
            $q->select('id', 'name', 'mobile');
        }]);

        $query->with(['product' => function ($q) {
            $q->select('id', 'name');
        }]);

        $orders = $query->paginate($request->per_page ?? 50);
        return OrderResource::collection($orders);
    }

    // نمایش لیست جدیدترین سفارشات
    public function new_orders(Request $request)
    {
        $request->validate([
            'last_id' => 'required|numeric|exists:orders,id'
        ]);
        $orders = Order::query()->admin_order()->get();
        return OrderResource::collection($orders);
    }

    // نمایش جزئیات سفارش کاربر
    public function show($order_id)
    {
        $order = Order::query()
            ->admin_order()
            ->where('id', $order_id)
            ->with(['user' => function ($q) {
                $q->select('id', 'name', 'mobile');
            }])
            ->with(['product' => function ($q) {
                $q->select('id', 'name');
            }])
            ->firstOrFail();

        return new OrderResource($order);
    }

    // تایید یا رد کردن سفارش
    public function confirm_reject(Request $request)
    {
        $request->validate([
            'order_id' => 'required|exists:orders,id',
            'status' => 'required|in:-1,1'
        ]);
        try {
            $order = Order::query()
                ->admin_order()
                ->findOrFail($request->order_id);
            $order->update($request->only('status'));

            OrderChangeStatus::dispatch($order);

            return Response::success();

        } catch (\Exception $exception) {
            return Response::error();
        }
    }

    // اضافه کردن دستی سفارش برای کاربر
    public function add_order(Request $request)
    {
        $request->validate([
            'mobile' => 'required|exists:users,mobile',
            'type' => 'required|in:sell,buy',
            'amount' => 'required|numeric',
            'product_id' => 'required|exists:products,id',
        ]);
        try {
            $user = User::query()->where('mobile', $request->mobile)->first();
            $product = Product::query()->where('id', $request->product_id)->first();

            $product->orders()->create([
                'type' => $request->type,
                'amount' => $request->amount,
                'user_id' => $user->id,
                'store_id' => $product->store_id,
                'price' => $product["{$request->type}_price"],
            ]);
            return Response::success();
        } catch (\Exception $exception) {
            return Response::error();
        }
    }
}
