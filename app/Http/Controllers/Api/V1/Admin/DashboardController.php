<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Api\Response;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $user_count = User::query()->count();
        $active_users = User::query()->active()->count();
        $products_count = Product::query()->count();
        $last_users = User::query()->take(10)->get();
        $last_users = UserResource::collection($last_users);

        $today_order = Order::query()
            ->withoutGlobalScopes()
            ->whereDate('created_at', Carbon::today())
            ->groupBy('type')->select([
                'type',
                DB::raw('count(*) as count'),
                DB::raw('sum(' . DB::raw('amount * ' . DB::raw('IF(orders.type="buy", 1, -1)')) . ') as total_amount'),
                DB::raw('sum(' . DB::raw('amount * price * ' . DB::raw('IF(orders.type="buy", 1, -1)')) . ') as total_price')
            ])->get();

        $monthly_order = Order::query()
            ->withoutGlobalScopes()
            ->whereDate('created_at', '>', Carbon::now()->subMonths(1))
            ->groupBy('type')->select([
                'type',
                DB::raw('count(*) as count'),
                DB::raw('sum(' . DB::raw('amount * ' . DB::raw('IF(orders.type="buy", 1, -1)')) . ') as total_amount'),
                DB::raw('sum(' . DB::raw('amount * price * ' . DB::raw('IF(orders.type="buy", 1, -1)')) . ') as total_price')
            ])->get();

        $yearly_order = Order::query()
            ->withoutGlobalScopes()
            ->whereDate('created_at', '>', Carbon::now()->subyears(1))
            ->groupBy('type')->select([
                'type',
                DB::raw('count(*) as count'),
                DB::raw('sum(' . DB::raw('amount * ' . DB::raw('IF(orders.type="buy", 1, -1)')) . ') as total_amount'),
                DB::raw('sum(' . DB::raw('amount * price * ' . DB::raw('IF(orders.type="buy", 1, -1)')) . ') as total_price')
            ])->get();

        $total_order = Order::query()
            ->withoutGlobalScopes()
            ->groupBy('type')->select([
                'type',
                DB::raw('count(*) as count'),
                DB::raw('sum(' . DB::raw('amount * ' . DB::raw('IF(orders.type="buy", 1, -1)')) . ') as total_amount'),
                DB::raw('sum(' . DB::raw('amount * price * ' . DB::raw('IF(orders.type="buy", 1, -1)')) . ') as total_price')
            ])->get();

        return Response::success(compact(
            'today_order', 'monthly_order', 'yearly_order', 'total_order',
            'user_count', 'active_users', 'products_count', 'last_users'
        ));
    }
}
