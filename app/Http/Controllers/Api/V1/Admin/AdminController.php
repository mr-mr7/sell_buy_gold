<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\AdminResource;

class AdminController extends Controller
{
    //  گرفتن ادمین وارد شده
    public function current()
    {
        return new AdminResource(auth('admin')->user());
    }
}
