<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\AdminResource;
use App\Http\Resources\OperatorResource;
use App\Http\Resources\UserResource;
use App\Models\Admin;
use App\Models\User;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /*
     * نمایش اطلاعات یوزر جاری
     */
    public function current()
    {
        if (auth()->user() instanceof User) {
            return new UserResource(auth('user')->user());
        } else if (auth()->user() instanceof Admin) {
            return new AdminResource(auth('admin')->user());
        } else {
            return new OperatorResource(auth('operator')->user());
        }

    }
}
