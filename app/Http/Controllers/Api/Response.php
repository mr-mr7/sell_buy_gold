<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Response extends Controller
{
    /*
     * وقتی پاسخ عملیات موفقیت امیز باشد
     */
    public static function success($data = null, $message = 'عملیات با موفقیت انجام شد!', $status = 200): \Illuminate\Http\JsonResponse
    {
        $response = [
            'data' => $data,
            'message' => $message,
        ];
        if (is_null($data)) unset($response['data']);
        return response()->json($response, $status);
    }

    /*
     * وقتی عملیات با شکست مواجه شود
     */
    public static function error($message = 'مشکلی بوجود آمده است لطفا بعدا تلاش کنید!', $status = 500)
    {
        $response = [
            'message' => $message,
        ];
        return response()->json($response, $status);
    }
}
