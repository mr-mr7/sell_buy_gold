<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Models\BotUser;
use App\Traits\Telegram\Callback;
use App\Traits\Telegram\Inline;
use App\Traits\Telegram\Mr7Telegram;
use App\Traits\Telegram\Step;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramController extends Controller
{
    use Mr7Telegram,Callback, Inline, Step;
    private $updates;

    public function webhook_updates()
    {
        $this->updates = Telegram::getWebhookUpdates();
        $this->set_attributes();
        /*Telegram::sendMessage([
            'chat_id' => 533787134,
            'text' => $this->text
        ]);*/
        Telegram::commandsHandler(true);

        $this->CallbackHandler();
        $this->InlineHandler();
        $this->StepHandler();
    }
}
