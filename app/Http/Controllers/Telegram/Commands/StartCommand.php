<?php


namespace App\Http\Controllers\Telegram\Commands;

use App\Models\BotUser;
use App\Traits\Telegram\Mr7Telegram;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

class StartCommand extends Command
{
    use Mr7Telegram;
    /**
     * @var string Command Name
     */
    protected $name = "start";

    /**
     * @var string Command Description
     */
    protected $description = "Start Command to get you started";

    /**
     * @inheritdoc
     */
    public function handle()
    {
        $this->set_attributes();
        if ($this->is_login) {
            $keyboard = [
//                [$this->keyboard('track_order'), $this->keyboard('orders_list')],
                [$this->keyboard('orders_list')],
                [$this->keyboard('contact_us'),$this->keyboard('logout')],
            ];
            $text= 'به ربات هانی گلد خوش آمدید';
        }else {
            $keyboard = [
                [$this->keyboard('login'), $this->keyboard('register')],
                [$this->keyboard('contact_us'), $this->keyboard('about_us')],
            ];
            $text= 'به ربات هانی گلد خوش آمدید. جهت استفاده از امکانات ربات لطفا وارد شوید';
        }
        $reply_markup = Keyboard::make([
            'keyboard' => $keyboard,
            'resize_keyboard' => false,
            'one_time_keyboard' => true
        ]);

        $this->replyWithMessage([
            'text' => $text,
            'reply_markup' => $reply_markup
        ]);
        BotUser::update_step('');
        exit();
    }
}
