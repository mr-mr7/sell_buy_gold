<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Api\Response;
use App\Models\Setting;
use Closure;
use Illuminate\Http\Request;

class SiteStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     */
    public function handle(Request $request, Closure $next)
    {
        $site_status= Setting::query()->where('name','site_status')->value('value');
        if ($site_status == '0') {
            return Response::error('سایت بسته میباشد. ثبت سفارش در حال حاضر امکان پذیر نمیباشد',404);
        }
        return $next($request);
    }
}
