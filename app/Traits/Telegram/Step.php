<?php


namespace App\Traits\Telegram;


use App\Http\Controllers\Telegram\Commands\StartCommand;
use App\Models\BotUser;
use App\Models\User;
use App\Notifications\VerifySms;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

trait Step
{
    private function StepHandler()
    {
        $step = $this->user->step;
        if ($step && method_exists(self::class,$step)) {
            $this->{$step}();
            exit();
        }
    }

    // ارسال کد تایید به موبایل کاربر
    private function send_verify_sms()
    {
        $user = User::query()->where('mobile', intval($this->text))->active()->first(['id']);
        if ($user) {
            $user_bot= BotUser::query()->this_user()->first();
            $user_bot->notify(new VerifySms(['user_id' => $user->id]));
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => 'کد تایید به شماره موبایل شما ارسال شد',
            ]);
            BotUser::query()->this_user()->update(['step' => 'verify_sms']);
        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => 'اکانتی با این شماره وجود ندارد یا هنوز توسط ادمین تایید نشده است',
            ]);
            BotUser::update_step('');
        }
    }


    // تایید کد
    private function verify_sms()
    {
        $user_bot= BotUser::query()->this_user()->first();
        $verify_notif= $user_bot->verify_notif;
        if (!$verify_notif) {
            $keyboard = Keyboard::make()
                ->inline()
                ->row(
                    Keyboard::inlineButton(['text' => 'دریافت مجدد کد تایید', 'callback_data' => 'resend_verify_code']),
                );
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => "کد تایید شما منقضی شده است. \n جهت دریافت مجدد کد تایید روی باتن زیر کلیک کنید \n .",
                'reply_markup' => $keyboard
            ]);

        }
        else if ($verify_notif['code'] != $this->text) {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => 'کد تایید اشتباه میباشد',
            ]);
            // میتونه یه انلاین باتن باشه که بگه ارسال مجدد کد فعالسازی
        }else {
            $user_id= $verify_notif['user_id'];
            $user_bot->user_id= $user_id;
            $user_bot->step= '';
            $user_bot->save();
            $user_bot->deleteVerifyCode();
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => 'شما با موفقیت وارد شدید',
            ]);
            Telegram::triggerCommand('start',$this->updates);
        }
    }
}
