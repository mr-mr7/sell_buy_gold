<?php


namespace App\Traits\Telegram;


use App\Models\BotUser;
use App\Models\User;
use App\Notifications\VerifySms;

trait Inline
{
    private function InlineHandler()
    {
        if ($this->callbak_data) {
            $params = explode('#',$this->callbak_data);
            $method= $params[0];
            if ($method && method_exists(self::class, $method)) {
                BotUser::update_step('');
                unset($params[0]);
                $this->{$method}($params);
                exit();
            }
        }
    }

    private function resend_verify_code($params=[])
    {
        $this->login();
    }
}
