<?php

namespace App\Traits\Telegram;

use App\Models\BotUser;
use App\Models\User;

trait Mr7Telegram
{
    protected $user_id, $chat_id, $message_id, $text, $is_login, $user, $callbak_data, $callbak_id;

    // ست کردن اتریبیوت های کاربردی در ربات
    private function set_attributes()
    {
        if (request()->has('callback_query')) { // inline button
            $this->user_id = request()->callback_query['from']['id'];
            $this->chat_id = request()->callback_query['message']['chat']['id'];
            $this->message_id = request()->callback_query['message']['message_id'];
            $this->text = request()->callback_query['message']['text'];

            $this->callbak_data = request()->callback_query['data'];
            $this->callbak_id = request()->callback_query['id'];
        } else {
            $this->user_id = request()->message['from']['id'];
            $this->chat_id = request()->message['chat']['id'];
            $this->message_id = request()->message['message_id'];
            $this->text = request()->message['text'];
        }

        $this->set_user();
        $this->is_login = $this->check_login();

    }

    // بررسی اینکه کاربر لاگین هست یا ن
    private function check_login()
    {
        return $this->user->mobile ? true : false;
    }

    private function keyboards_array()
    {
        return [
            'orders_list' => 'مشاهده سفارشات',
            'track_order' => 'پیگیری سفارش',
            'contact_us' => 'تماس با ما',
            'register' => 'ثبت نام',
            'logout' => 'خروج',
            'login' => 'ورود',
            'about_us' => 'درباره ما',
        ];
    }

    // این تابع برای گرفتن نام فارسی دکمه های ربات میباشد
    public function keyboard($key)
    {
        $array = $this->keyboards_array();
        return $array[$key] ?? '';
    }

    // این تابه ما مقدار فارسی بهش میدیم و اون key اون رو بهمون میده
    public function key_keyboard($val)
    {
        $array = array_flip($this->keyboards_array());
        return $array[$val] ?? '';
    }

    // ثبت ربات اگه تازه اومده
    private function set_user()
    {
        $user = BotUser::query()
            ->withoutGlobalScopes()
            ->where('userbot_id', $this->user_id)
            ->leftJoin('users', 'users.id', 'bot_users.user_id')
            ->first();
        if ($user) {
            $this->user = $user;
        } else {
            $this->user = BotUser::query()->create([
                'userbot_id' => $this->user_id
            ]);
        }
    }

}
