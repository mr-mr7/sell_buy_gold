<?php


namespace App\Traits\Telegram;


use App\Http\Controllers\Telegram\Commands\StartCommand;
use App\Models\BotUser;
use App\Models\Order;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;

trait Callback
{
    private function CallbackHandler()
    {
        $method = $this->key_keyboard($this->text);
        if ($method && method_exists(self::class, $method)) {
            BotUser::update_step('');
            $this->{$method}();
            exit();
        }
    }

    private function login()
    {
        if ($this->is_login) {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => 'شما قبلا وارد شده اید'
            ]);
            Telegram::triggerCommand('start',$this->updates);
            exit();
        }
        Telegram::sendMessage([
            'chat_id' => $this->chat_id,
            'text' => 'جهت ورود شماره خود را وارد کنید'
        ]);
        BotUser::update_step('send_verify_sms');
    }

    private function register()
    {
        $keyboard = Keyboard::make()
            ->inline()
            ->row(
                Keyboard::inlineButton(['text' => 'ثبت نام در سایت', 'url' => 'https://hani.gold/user/register']),
            );
        Telegram::sendMessage([
            'chat_id' => $this->chat_id,
            'text' => 'لطفا برای ثبت نام روی باتن زیر کلیک کنید',
            'reply_markup' => $keyboard
        ]);
    }

    private function orders_list()
    {
        $user_id = $this->user_id;
        $orders = Order::query()
            ->whereHas('user.bot_users', function ($q) use ($user_id) {
                $q->where('userbot_id', $user_id);
            })
            ->with('product')
            ->take(10)->get();
        if ($orders->count() > 0) {
            $text = "<b>لیست سفارشات اخیر شما: </b> \n";
            foreach ($orders as $key=>$order) {
                $price= number_format($order->price);
                $text .= ($key+1).") <b>{$order->status_text}:</b> {$order->type} {$order->amount} {$order->product->unit} {$order->product->name} با قیمت {$price} ریال ";
                $text .= "\n";
            }
            $text .=' .';
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => $text,
                'parse_mode' => "HTML"
            ]);

            $keyboard = Keyboard::make()
                ->inline()
                ->row(
                    Keyboard::inlineButton(['text' => 'مشاهده لیست کامل سفارشات', 'url' => 'https://hani.gold/api/v1/user/orders']),
                );
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => 'برای مشاهده لیست کامل سفارشات روی باتن زیر کلیک کنید',
                'reply_markup' => $keyboard
            ]);


        } else {
            Telegram::sendMessage([
                'chat_id' => $this->chat_id,
                'text' => 'در حال حاضر سفارشی برای شما ثبت نشده است'
            ]);
        }
    }

    private function track_order()
    {

    }

    private function contact_us()
    {
        Telegram::sendMessage([
            'chat_id' => $this->chat_id,
            'text' => 'در اینجا متن و اطلاعات تماس با ما نوشته میشود'
        ]);
    }

    private function about_us()
    {
        Telegram::sendMessage([
            'chat_id' => $this->chat_id,
            'text' => 'در اینجا متن درباره ما نوشته میشود'
        ]);
    }

    private function logout()
    {
        BotUser::query()->where('userbot_id', $this->user_id)->update([
            'step' => '',
            'user_id' => NULL
        ]);
        Telegram::sendMessage([
            'chat_id' => $this->chat_id,
            'text' => 'شما با موفقیت خارج شدید'
        ]);
        Telegram::triggerCommand('start',$this->updates);
    }


}
