<?php


namespace App\Traits;


trait VerifyCode
{
    public function getVerifyCodeAttribute()
    {
        $notification= $this->notifications()
            ->where('type', 'App\Notifications\VerifySms')
            ->where('created_at', '>', now()->addMinutes(-2))
            ->latest()
            ->first();
        if ($notification) {
            return $notification->data['code'];
        }
        return false;
    }

    public function deleteVerifyCode()
    {
        $this->notifications()
            ->where('type', 'App\Notifications\VerifySms')
            ->delete();
    }

    public function getVerifyNotifAttribute()
    {
        $notification= $this->notifications()
            ->where('type', 'App\Notifications\VerifySms')
            ->where('created_at', '>', now()->addMinutes(-2))
            ->latest()
            ->first();

        if ($notification) {
            return $notification->data;
        }
        return false;

    }

}
