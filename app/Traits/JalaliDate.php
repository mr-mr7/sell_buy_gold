<?php


namespace App\Traits;


trait JalaliDate
{
    public function getCreatedAtAttribute()
    {
        return $this->attributes['created_at'] ? jdate($this->attributes['created_at'])->format('Y/m/d H:i') : null;
    }
    public function getUpdatedAtAttribute()
    {
        return $this->attributes['updated_at'] ? jdate($this->attributes['updated_at'])->format('Y/m/d H:i') : null;
    }
}
