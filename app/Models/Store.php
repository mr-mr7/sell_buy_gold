<?php

namespace App\Models;

use App\Scopes\OrderScope;
use App\Traits\JalaliDate;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory,JalaliDate;
    protected $guarded= [];

    protected static function boot() {
        parent::boot();
        static::addGlobalScope(new OrderScope('created_at', 'desc'));
    }

    // Relations
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function admins()
    {
        return $this->hasMany(Admin::class);
    }

    public function operators()
    {
        return $this->hasMany(Operator::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    //## Relations
}
