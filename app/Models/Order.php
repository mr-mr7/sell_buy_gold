<?php

namespace App\Models;

use App\Scopes\OrderScope;
use App\Traits\JalaliDate;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    // UPDATE orders JOIN bot_users on bot_users.userbot_id = orders.user_id set orders.user_id=bot_users.user_id
    // فروشگاه فلان قدر از فلان محصول رو از مشتری خریده (buy) / فروخته (sell)
    // اینجوری نوشتم چون برای گرفتن تعداد سفارشات فروش یا خرید فروشگاه یا فرمول تراز یا هر فرمولی که بخش ادمین نیاز داره راحت تره

    // buy: یعنی فروشگاه خریده و مشتری فروخته
    // sell: یعنی فروشگاه فروخته و مشتری خریده

    // وقتی قراره سفارش ثبت بشه :
    // مشتری داره میخره یعنی فروشگاه داره میفروشه پس نوع سفارش میشه sell و قیمت فروش محصول بخوره
    // مشتری داره میفروشه یعنی فروشگاه داره میخره نوع سفارش میشه buy و قیمت خرید محصول بخوره
    /*
     * status => 0:new | 1:confirmed | -1: reject
     */
    use HasFactory, JalaliDate;

    protected $table = 'orders';
    protected $guarded = [];

    protected static function boot() {
        parent::boot();
        static::addGlobalScope(new OrderScope('orders.created_at', 'desc'));
    }

    // Relations
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
    //## Relations

    // Scopes
    // این اسکوپ برای مشخص کردن سفارشاتی هست که فقط باید ادمین هر فروشگاه ببینه
    public function scopeAdmin_order($q)
    {
        if (auth('admin')->user()->hasRole('super admin')) return $q;
        return $q->whereHas('product', function ($q) {
            $q->where('store_id', auth('admin')->user()->store_id);
        });
    }
    //## Scopes


    // Mutators
    public function getTypeAttribute()
    {
        if (auth('user')->user()) {
            // sell یعنی فروشگاه فروخته و کاربر خریده
            // پس توی لیست کاربر باید بهش خرید نشون بدیم
            return $this->attributes['type'] == 'sell' ? 'خرید' : 'فروش';
        }
        return $this->attributes['type'] == 'buy' ? 'خرید' : 'فروش';
    }

    public function getStatusTextAttribute()
    {
        switch ($this->status) {
            case '-1': return 'رد شده';
            case '0': return 'جدید';
            case '1': return 'تایید شده';
        }
        return '';
    }
    // ##Mutators
}
