<?php

namespace App\Models;

use App\Scopes\OrderScope;
use App\Traits\JalaliDate;
use App\Traits\VerifyCode;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class Operator extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, VerifyCode, JalaliDate;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'mobile',
    ];


    protected static function boot() {
        parent::boot();
        static::addGlobalScope(new OrderScope('created_at', 'desc'));
    }

    // Methods
    public function routeNotificationForKavenegar($driver, $notification = null)
    {
        return $this->mobile;
    }
    // ## Methods
}
