<?php

namespace App\Models;

use App\Scopes\OrderScope;
use App\Traits\JalaliDate;
use App\Traits\VerifyCode;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class BotUser extends Model
{
    use HasFactory, Notifiable, VerifyCode, JalaliDate;

    protected $guarded = [];

    protected $primaryKey = 'userbot_id';

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OrderScope('created_at', 'desc'));
    }

    // Relations

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //## Relations

    // Scope

    public function scopeThis_user($q)
    {
        $user_id = request()->has('callback_query') ? request()->callback_query['from']['id'] : request()->message['from']['id'];
        return $q->where('userbot_id', $user_id);
    }

    // ## Scope

    // Method
    public static function update_step($step)
    {
        $user_id = request()->has('callback_query') ? request()->callback_query['from']['id'] : request()->message['from']['id'];
        return self::query()->where('userbot_id',$user_id)
            ->update([
                'step' => "$step"
            ]);
    }
    // ## Method
}
