<?php

namespace App\Models;

use App\Scopes\OrderScope;
use App\Traits\JalaliDate;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, JalaliDate;
    protected $guarded= [];

    protected static function boot() {
        parent::boot();
        static::addGlobalScope(new OrderScope('created_at', 'desc'));
    }

    // Relations
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    // این اسکوپ برای مشخص کردن سفارشاتی هست که فقط باید
    public function scopeProducts_store($q)
    {
        if (auth('admin')->user()->hasRole('super admin')) return $q;
        return $q->where('store_id',auth()->user()->store_id ?? null);// Operator | Admin Store
    }
    //## Relations
}
