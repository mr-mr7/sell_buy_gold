<?php

namespace App\Models;

use App\Scopes\OrderScope;
use App\Traits\JalaliDate;
use App\Traits\VerifyCode;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, VerifyCode, JalaliDate;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
//    protected $fillable = [
//        'name',
//        'mobile',
//    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OrderScope('created_at', 'desc'));
    }

    protected $guarded = [];

    // Scopes
    public function scopeActive($q)
    {
        return $q->where('status', 1);
    }
    //## Scopes

    // Relations
    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id', 'id');
    }

    public function bot_users()
    {
        return $this->hasMany(BotUser::class, 'user_id', 'id');
    }
    // ## Relations

    // Methods
    public function routeNotificationForKavenegar($driver, $notification = null)
    {
        return $this->mobile;
    }
    // ## Methods


    // Mutators
    // تراز تجمیعی کاربر
    public function getLevelAttribute()
    {
        return 1156885698;
    }

    // تراز محصولات کاربر
    public function getProductsLevelAttribute()
    {
        $product_level = Product::query()
            ->withoutGlobalScope(OrderScope::class)
            ->join('orders', 'orders.product_id', 'products.id')
            ->where('orders.user_id', $this->id)
            ->select([
                'products.id',
                'products.name',
                'products.unit',
                DB::raw('sum(' . DB::raw('amount * ' . DB::raw('IF(orders.type="sell", -1, 1)')) . ') as level'),
                DB::raw('sum(' . DB::raw('amount * price * ' . DB::raw('IF(orders.type="buy", 1, -1)')) . ') as level_price')
            ])
            ->groupBy('product_id')
            ->get();
        return $product_level;
    }

    // گرفتن آخرین یوزر ایدی (ربات) کاربر
    public function getUserbotIdAttribute()
    {
        return $this->bot_users()->latest()->value('userbot_id');
    }
    //## Mutators
}
