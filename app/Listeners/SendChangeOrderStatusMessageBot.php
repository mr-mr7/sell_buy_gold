<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Telegram\Bot\Laravel\Facades\Telegram;

class SendChangeOrderStatusMessageBot implements ShouldQueue
{
    use InteractsWithQueue;

    public $queue = 'listeners';
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $order= $event->order;
        $status_fa = $order->status == 1 ? 'تایید' : 'رد';
        $sell_buy_fa = $order->type == 'sell' ? 'فروش' : 'خرید';
        $name = $order->name == 'coin' ? 'عدد سکه' : 'گرم آبشده';
        $userbot_id = $order->user->bot_users()->first(['user_id', 'userbot_id']);
        if ($userbot_id) {
            $userbot_id = $userbot_id->userbot_id;
            $text = "*" . "سفارش شما " . $status_fa . " شد " . "*";
            $text .= "\n➖➖➖➖➖➖➖➖➖➖➖➖➖\n";
            $text .= "توضیحات سفارش شما: \n";
            $text .= $sell_buy_fa . " " . floatval($order['amount']) . " " . $name . " در تاریخ " . jdate($order['created_at'])->format('Y/m/d') . " ساعت " . jdate($order['created_at'])->format('H:i:s') . " \n";

            Telegram::setAsyncRequest(true)
                ->sendMessage([
                    'chat_id' => $userbot_id,
                    'text' => $text
                ]);
        }
    }
}
