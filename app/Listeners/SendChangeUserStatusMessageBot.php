<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Telegram\Bot\Laravel\Facades\Telegram;

class SendChangeUserStatusMessageBot implements ShouldQueue
{
    use InteractsWithQueue;

    public $queue = 'listeners';
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        $user = $event->user;
        $userbot_id = $user->bot_users()->first(['user_id', 'userbot_id']);
        if ($userbot_id) {
            $userbot_id = $userbot_id->userbot_id;
            $status_fa = $user->status == 1 ? 'تایید' : 'رد';
            $text = "*" . "عضویت شما در ربات حانی گلد " . $status_fa . " شد " . "*";
            $text .= "\n➖➖➖➖➖➖➖➖➖➖➖➖➖\n";
            $text .= "جهت پیگیری بیشتر با مدیریت ربات حانی گلد در ارتباط باشید. \n";

            Telegram::setAsyncRequest(true)
                ->sendMessage([
                    'chat_id' => $userbot_id,
                    'text' => $text
                ]);
        }
    }
}
