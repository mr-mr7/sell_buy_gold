<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Kavenegar\Laravel\Message\KavenegarMessage;
use Kavenegar\Laravel\Notification\KavenegarBaseNotification;

class VerifySms extends KavenegarBaseNotification
{
    use Queueable;

    /**
     * @var User
     */
    private $code;
    private $data;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->code = rand(10000, 99999);
        $this->data = $data;
    }

    public function toKavenegar($notifiable)
    {
//        return true;
        return (new KavenegarMessage())
            ->verifyLookup('haniverify', [$this->code]);
    }

    public function toArray($notifiable)
    {
        return array_merge(['code' => $this->code],$this->data);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
//        return ['database'];
        return ['database', 'kavenegar'];
    }
}
