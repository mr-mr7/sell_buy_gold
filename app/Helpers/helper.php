<?php
function calc_user_level($product_level) {
    $level = 0;
    foreach ($product_level as $item) {
        $level += $item->level_price;
    }
    return $level;
}
