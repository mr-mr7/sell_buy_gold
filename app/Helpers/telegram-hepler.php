<?php

use App\Models\User;

// بررسی اینکه کاربر لاگین هست یا ن
function BOT_check_login($user_id)
{
    return User::query()->where('userbot_id', $user_id)->count() > 0;
}

function BOT_keyboard($key) {
    $array= [
        'orders-list' => 'مشاهده سفارشات',
        'track-order' => 'پیگیری سفارش',
        'contact-us' => 'تماس با ما',
        'logout' => 'خروج',
        'login' => 'ورود',
        'register' => 'ثبت نام',
        'about-us' => 'درباره ما',
    ];
    return $array[$key] ?? '';
}

function user_bot()
{
    return \App\Models\BotUser::query()->where('userbot_id',request()->message['from']['id'])->first();
}
