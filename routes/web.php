<?php

use App\Http\Controllers\Telegram\TelegramController;
use Illuminate\Support\Facades\Route;


// https://api.telegram.org/bot951680706:AAEWzt3KPMNBlyK5a1ClQueK_Sc1q67q4nA/setWebhook?url=https://b754-194-50-233-242.eu.ngrok.io/951680706:AAEWzt3KPMNBlyK5a1ClQueK_Sc1q67q4nA/webhook
// https://api.telegram.org/bot951680706:AAEWzt3KPMNBlyK5a1ClQueK_Sc1q67q4nA/deleteWebhook
// https://api.telegram.org/bot951680706:AAEWzt3KPMNBlyK5a1ClQueK_Sc1q67q4nA/getMe
// Get Webhook Update
Route::post(env('TELEGRAM_BOT_TOKEN') . "/webhook",[TelegramController::class,'webhook_updates']);
//Route::view('test-p','test-p');
