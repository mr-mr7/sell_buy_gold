<?php

use Illuminate\Support\Facades\Route;


Route::get('dashboard','DashboardController@dashboard');

Route::get('users','UserController@index')->middleware(['can:users list']);
Route::get('users/{user}','UserController@show')->middleware(['can:show user']);
Route::put('users/confirm-reject/{user}','UserController@confirm_reject')->middleware(['can:confirm reject user']);
Route::put('users/{user}/update','UserController@update')->middleware(['can:update user']);

Route::get('orders','OrderController@index')->middleware(['can:orders list']);
Route::get('orders/{order}','OrderController@show')->middleware(['can:show order']);
Route::put('orders/confirm-reject','OrderController@confirm_reject')->middleware(['can:confirm reject order']);
Route::post('orders/add','OrderController@add_order')->middleware(['can:add order']);
Route::post('orders/new','OrderController@new_orders')->middleware(['can:orders list']);

Route::get('products','ProductController@index')->middleware(['can:products list']);
Route::put('products/{product}','ProductController@update')->middleware(['can:update product']);

Route::get('settings','SettingController@index')->middleware(['can:settings list']);
Route::put('settings','SettingController@update')->middleware(['can:update settings']);


Route::get('level','LevelController@index')->middleware(['can:level index']);

Route::post('logout','AuthController@logout');

