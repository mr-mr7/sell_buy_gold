<?php

use Illuminate\Support\Facades\Route;

Route::get('products','ProductController@index')->middleware(['can:products list']);

Route::get('orders','OrderController@index')->middleware(['can:orders list']);
Route::get('orders/{order}','OrderController@show')->middleware(['can:show order']);
Route::post('orders','OrderController@store')->middleware(['can:new order','site.status']);


Route::post('logout','AuthController@logout');
