<?php

use Illuminate\Support\Facades\Route;

Route::get('settings/general','SettingController@general')->middleware(['can:show general settings']);
Route::put('settings/general','SettingController@general_store')->middleware(['can:store general settings']);

Route::get('products','ProductController@index')->middleware(['can:products list']);
