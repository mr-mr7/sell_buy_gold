<?php

use App\Http\Controllers\Api\V1\ApiController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'guest:user',
], function (){
    Route::post('user/login', 'User\AuthController@login');
    Route::post('user/register', 'User\AuthController@register');
});

Route::group([
    'middleware' => 'guest:admin',
], function (){
    Route::post('admin/login', 'Admin\AuthController@login');
});

Route::group([
    'middleware' => 'guest:operator',
], function (){
    Route::post('operator/login', 'Operator\AuthController@login');
});

Route::get('current',[ApiController::class,'current'])->name('current.user')->middleware(['auth:user,admin,operator']);

