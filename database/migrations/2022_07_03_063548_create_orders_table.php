<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->enum('type',['buy','sell'])->comment(' فروش (sell) یعنی اینکه فروشگاه فروخته و خرید (buy) یعنی فروشگاه این محصولو خریده');
            $table->double('amount',20,8,true);
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('store_id');
//            $table->enum('name',['coin','melted'])->default('coin');
            $table->integer('price')->unsigned();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();

            $table->foreign('user_id')
                ->on('users')
                ->references('id');

            $table->foreign('product_id')
                ->on('products')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
