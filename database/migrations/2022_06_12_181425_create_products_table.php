<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('unit');
            $table->unsignedBigInteger('store_id');
            $table->unsignedInteger('buy_price')->comment('قیمت خرید فروشگاه');
            $table->unsignedInteger('sell_price')->comment('قیمت فروش فروشگاه');
            $table->unsignedFloat('limit')->comment('سقف هربار سفارش');
            $table->timestamps();

            $table->foreign('store_id')
                ->on('stores')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
