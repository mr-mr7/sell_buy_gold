<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::truncate();

        $admin= Admin::query()->create([
            'name' => 'سوپر ادمین',
            'mobile' => 9136940239,
        ]);
        $admin->assignRole('super admin');

        $admin= Admin::query()->create([
            'name' => 'ادمین فروشگاه هانی گلد',
            'mobile' => 9372549157,
            'store_id' => 1,
        ]);
        $admin->assignRole('admin');
    }
}
