<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\UserBot;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         \App\Models\User::factory(10)->create();
        Schema::disableForeignKeyConstraints();
        $this->call([
            PermissionSeeder::class,
            RoleSeeder::class,
            StoreSeeder::class,
            UserSeeder::class,
            UserBotSeeder::class,
            AdminSeeder::class,
            OperatorSeeder::class,
            ProductSeeder::class,
            OrderSeeder::class,
            SettingSeeder::class,
            OauthClientSeeder::class,
        ]);
        Schema::enableForeignKeyConstraints();
    }
}
