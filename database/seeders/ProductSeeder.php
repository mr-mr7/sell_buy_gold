<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();
        Product::query()->insert([
            ['name' => 'سکه', 'unit' => 'عدد', 'limit'=>20, 'created_at' => now(), 'store_id' => 1, 'sell_price' => 5, 'buy_price' => 55],
            ['name' => 'آبشده', 'unit' => 'گرم', 'limit'=>200, 'created_at' => now(), 'store_id' => 1, 'sell_price' => 6, 'buy_price' => 55],
//            ['name' => 'محصول جدیدی', 'created_at' => now(), 'store_id' => 2, 'sell_price' =>6, 'buy_price' => 55],
        ]);
    }
}
