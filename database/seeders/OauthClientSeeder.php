<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;

class OauthClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $str1= Str::random(40);
        $str2= Str::random(40);
        $str3= Str::random(40);
        DB::table('oauth_clients')->truncate();
        DB::insert("INSERT INTO oauth_clients (`name`,`secret`,`provider`,`redirect`,`personal_access_client`,`password_client`,`revoked`) VALUES
                        ('user','{$str1}','users','',1,0,false ),
                        ('admin','{$str2}','admins','',1,0,false ),
                        ('operator','{$str3}','operators','',1,0,false );
                    ");

    }
}
