<?php

namespace Database\Seeders;

use App\Models\Store;
use Illuminate\Database\Seeder;

class StoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Store::truncate();
        Store::query()->create([
            'name' => 'هانی گلد',
        ]);
        Store::query()->create([
            'name' => 'فروشگاه تست 2',
        ]);
    }
}
