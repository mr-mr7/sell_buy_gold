<?php

namespace Database\Seeders;

use App\Models\Operator;
use Illuminate\Database\Seeder;

class OperatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Operator::truncate();
        Operator::query()->create([
            'name' => 'اپراتور فروشگاه هانی گلد',
            'mobile' => 9372549157,
            'store_id' => 1
        ]);
    }
}
