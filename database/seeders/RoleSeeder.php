<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();


        Role::query()->create(['guard_name' => 'admin', 'name' => 'super admin']);
        $admin = Role::query()->create(['guard_name' => 'admin', 'name' => 'admin']);
        $operator = Role::query()->create(['guard_name' => 'operator', 'name' => 'operator']);
        $user = Role::query()->create(['guard_name' => 'user', 'name' => 'user']);

        // All Permission
        $permissions = Permission::all();

        // User Permission
        $user_permissions = $permissions->whereIn('name',
            [
                'products list',
                'orders list', 'show order', 'new order'
            ]
        )->where('guard_name','user');
        $user->syncPermissions($user_permissions);

        // Admin Permission
        $admin_permissions = $permissions->whereIn('name',
            [
                'show general settings', 'store general settings',
                'products list','update product',
                'orders list', 'show order', 'confirm reject order', 'add order',
                'users list', 'show user'
            ]
        )->where('guard_name','admin');
        $admin->syncPermissions($admin_permissions);

        // Operator Permission
        $operator_permissions = $permissions->whereIn('name',
            [
                'products list',
                'users list', 'show user', 'new order'
            ]
        )->where('guard_name','operator');
        $operator->syncPermissions($operator_permissions);

    }

}
