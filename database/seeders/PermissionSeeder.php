<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::truncate();
        Permission::query()->insert([
            //admin permission
            ['guard_name' => 'admin', 'name' => 'show general settings'],
            ['guard_name' => 'admin', 'name' => 'store general settings'],

            ['guard_name' => 'admin', 'name' => 'orders list'],
            ['guard_name' => 'admin', 'name' => 'show order'],
            ['guard_name' => 'admin', 'name' => 'confirm reject order'],
            ['guard_name' => 'admin', 'name' => 'add order'],

            ['guard_name' => 'admin', 'name' => 'users list'],
            ['guard_name' => 'admin', 'name' => 'show user'],
            ['guard_name' => 'admin', 'name' => 'confirm reject user'],
            ['guard_name' => 'admin', 'name' => 'update user'],

            ['guard_name' => 'admin', 'name' => 'products list'],
            ['guard_name' => 'admin', 'name' => 'update product'],

            ['guard_name' => 'admin', 'name' => 'settings list'],
            ['guard_name' => 'admin', 'name' => 'update settings'],

            ['guard_name' => 'admin', 'name' => 'level index'],

            // user permission
            ['guard_name' => 'user', 'name' => 'products list'],
            ['guard_name' => 'user', 'name' => 'orders list'],
            ['guard_name' => 'user', 'name' => 'show order'],
            ['guard_name' => 'user', 'name' => 'new order'],


            // operator permission
            ['guard_name' => 'operator', 'name' => 'products list'],
            ['guard_name' => 'operator', 'name' => 'users list'],
            ['guard_name' => 'operator', 'name' => 'show user'],
            ['guard_name' => 'operator', 'name' => 'new order'],

        ]);
    }
}
